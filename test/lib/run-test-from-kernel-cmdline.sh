#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# This script gets installed by run-test-pmbootstrap-qemu.sh into the VM. It
# figures out the test to run from the kernel command-line and runs ondev2 with
# expect and the test case.

TEST=""

for i in $(cat /proc/cmdline); do
	case "$i" in
		ONDEV2_TEST=*)
			TEST="$(echo "$i" | cut -d "=" -f 2)"
			;;
	esac
done

if [ -z "$TEST" ]; then
	echo "ERROR: ONDEV2_TEST not found in /proc/cmdline:"
	cat /proc/cmdline
	exit 1
fi

cd /opt

echo "Extracting /opt/tests.tar.gz"
tar -xvf tests.tar.gz

echo "Running test: $TEST"
expect -c "spawn ondev2" "test/cases/$TEST"
