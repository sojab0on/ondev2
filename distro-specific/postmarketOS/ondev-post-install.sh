#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Script to run when the installation is almost done. See defaults.cfg in
# ondev2.git for available environment variables.

if [ -z "$OD2" ]; then
	echo "ERROR: this script should only be called from inside ondev2"
	exit 1
fi

apk_chroot() {
	chroot "$OD2_CHROOT" apk --no-interactive --no-network "$@"
}

set -x

# Delete one of the postmarketos-fde-unlocker providers, based on whether FDE
# was chosen or not.
if [ "$OD2_FDE" = 1 ]; then
	apk_chroot del postmarketos-base-nofde
else
	apk_chroot del unl0kr
fi

chroot "$OD2_CHROOT" mkinitfs
