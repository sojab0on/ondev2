#!/bin/sh

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"

install -Dm755 "$SCRIPTDIR"/initfs-hook.sh \
	"$DESTDIR"/usr/share/mkinitfs/hooks-extra/ondev2.sh
install -Dm755 "$SCRIPTDIR"/initfs-hook.files \
	"$DESTDIR"/usr/share/mkinitfs/files-extra/ondev2.sh
install -Dm755 "$SCRIPTDIR"/ondev-prepare.sh \
	"$DESTDIR"/usr/bin/ondev-prepare
install -Dm755 "$SCRIPTDIR"/ondev-post-install.sh \
	"$DESTDIR"/usr/bin/ondev-post-install
