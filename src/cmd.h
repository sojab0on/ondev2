/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once

/* Convenience macro to run a command inside a shell, and return -1 on failure.
 * Changing the flow with return inside a macro should generally be avoided
 * (see checkpatch MACRO_WITH_FLOW_CONTROL). But in this case it de-duplicates
 * so much code that it's worth it. The "_or_ret" indicates that it returns on
 * error. */
#define run_or_ret(...) \
do { \
	if (run_cmd(__VA_ARGS__)) \
		return -1; \
} while (0)

int run_cmd(const char *format, ...);
