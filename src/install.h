/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include <stdbool.h>
#include <stdint.h>

/* Some steps are only enabled for specific use cases, see steps_init() */
enum od2_install_step_id {
	OD2_INSTALL_STEP_MOUNT_INSTALL_DATA,

	OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_CGPT,
	OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_REGULAR,
	OD2_INSTALL_STEP_FORMAT_BOOT_PARTITION_FS,

	/* Android use case: keep partition layout (repartition = false),
	 * combine multiple partitions via LVM */
	OD2_INSTALL_STEP_PREPARE_LVM,

	OD2_INSTALL_STEP_FORMAT_ROOT_PARTITION_LUKS,
	OD2_INSTALL_STEP_MOUNT_ROOT_PARTITION_LUKS,
	OD2_INSTALL_STEP_FORMAT_ROOT_PARTITION_FS,
	OD2_INSTALL_STEP_MOUNT_ROOT,
	OD2_INSTALL_STEP_COPY_FILES_ROOT,
	OD2_INSTALL_STEP_MOUNT_BOOT,
	OD2_INSTALL_STEP_COPY_FILES_BOOT,
	OD2_INSTALL_STEP_UPDATE_FSTAB,
	OD2_INSTALL_STEP_UPDATE_CRYPTTAB,
	OD2_INSTALL_STEP_PREPARE_CHROOT,
	OD2_INSTALL_STEP_RUN_POST_INSTALL_SCRIPT,

	OD2_INSTALL_STEP_CLEAN_CHROOT,
	OD2_INSTALL_STEP_UMOUNT_BOOT_ROOT_INSTALL_DATA,
	OD2_INSTALL_STEP_UMOUNT_ROOT_PARTITION_LUKS,

	/* Same install medium: delete the install data partition and extend
	 * root partition over the newly available free space */
	OD2_INSTALL_STEP_EXTEND_ROOT_OVER_INSTALL_DATA,

	_OD2_INSTALL_STEP_MAX,
};

struct od2_install_step {
	enum od2_install_step_id id;
	const char *name;
	const char *desc;
	uint8_t weight;
	bool enabled;
	int (*func)(); /* function from install_steps.h to run */
	int (*func_progress)(); /* optional function to get the progress */
};

void od2_install_start(void);
