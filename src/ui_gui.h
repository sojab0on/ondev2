/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include "lvgl/src/lv_api_map.h"
#include "pages.h"

void od2_ui_gui_init(void);
void od2_ui_gui_show_page(struct od2_page *page);

#define od2_ui_gui_interact() lv_task_handler()
