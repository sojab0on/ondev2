// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "pages.h"
#include "ui.h"
#include <fcntl.h>
#include <libintl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define _(str) gettext(str)
#define STDIN_LINE_MAXLEN 100

static struct {
	char stdin_buf[STDIN_LINE_MAXLEN - 1];
	char *stdin_pos;
	/* Callback for handling line typed in by user */
	void (*stdin_line_cb)(struct od2_page *page, const char *line);
} g_cli;

static void stdin_line_reset(void);
static void change_text_ask(struct od2_page *page);
static void change_text_cb(struct od2_page *page, const char *line);
static void what_now_cb(struct od2_page *page, const char *line);

static void print_dropdown(struct od2_page *page)
{
	struct od2_dropdown *dropdown;

	for (dropdown = page->dropdown; dropdown->id; dropdown++) {
		if (dropdown->hidden)
			continue;

		printf("%s: %s", dropdown->id, gettext(dropdown->name));

		if (dropdown == page->dropdown_selected)
			printf(" [%s]", _("selected"));

		printf("\n");
	}
	printf("\n");
}

static void print_text_input(struct od2_page *page)
{
	printf("%s: [", gettext(page->title));

	if (!page->input_text || !strlen(page->input_text)) {
		printf("(%s)", _("Empty"));
	} else if (page->is_password) {
		for (size_t i = 0; i < strlen(page->input_text); i++)
			putchar('*');
	} else {
		printf("%s", page->input_text);
	}

	printf("]\n");
	printf("c: %s\n", _("Change"));
	printf("\n");
}

static void print_progress_input(struct od2_page *page)
{
	printf("%s: %i%%", _("Progress"), page->progress);
	printf("\n");
}

static void print_buttons(struct od2_page *page)
{
	enum od2_button *button_id = page->buttons;
	uint8_t i;

	for (i = 0; button_id[i] != OD2_BUTTON_NONE; i++)
		printf("%d: %s\n", i, od2_page_get_button_text(button_id[i]));
	printf("\n");
}

static void what_now_ask(struct od2_page *page)
{
	if (page->input == OD2_INPUT_PROGRESS) {
		g_cli.stdin_line_cb = NULL;
		return;
	}

	printf("%s> ", _("What now?"));
	fflush(stdout);

	g_cli.stdin_line_cb = what_now_cb;
}

void what_now_cb(struct od2_page *page, const char *line)
{
	enum od2_button *button_id;
	struct od2_dropdown *dropdown;
	uint8_t i;

	/* Handle buttons */
	button_id = page->buttons;
	for (i = 0; button_id[i] != OD2_BUTTON_NONE; i++) {
		if (line[0] == '0' + i)
			return od2_ui_on_button(button_id[i]);
	}

	/* Handle dropdowns */
	for (dropdown = page->dropdown; dropdown && dropdown->id; dropdown++) {
		if (strcmp(line, dropdown->id) == 0)
			return od2_ui_on_dropdown(dropdown);
	}

	/* Handle text input */
	if (page->input == OD2_INPUT_TEXT) {
		if (line[0] == 'c') {
			change_text_ask(page);
			return;
		}
	}

	printf(_("Invalid selection.\n"));
	what_now_ask(page);
}

void od2_ui_cli_show_page(struct od2_page *page)
{
	printf("\n");
	printf(":: %s\n", gettext(page->title));
	printf("%s\n", od2_page_get_descr(page));

	printf("\n");

	if (page->dropdown)
		print_dropdown(page);
	if (page->input == OD2_INPUT_TEXT)
		print_text_input(page);
	else if (page->input == OD2_INPUT_PROGRESS)
		print_progress_input(page);

	print_buttons(page);

	what_now_ask(page);
	stdin_line_reset();
}

static void change_text_ask(struct od2_page *page)
{
	printf("%s: ", gettext(page->title));
	fflush(stdout);

	/* Hide typed characters */
	if (page->is_password)
		printf("\033[8m");

	g_cli.stdin_line_cb = change_text_cb;
}

static void change_text_cb(struct od2_page *page, const char *line)
{
	free(page->input_text);
	page->input_text = strdup(line);

	/* Show typed characters again */
	if (page->is_password)
		printf("\033[28m");

	od2_ui_show_page();
}

void stdin_line_reset(void)
{
	g_cli.stdin_pos = g_cli.stdin_buf;
}

size_t stdin_line_len(void)
{
	return g_cli.stdin_pos - g_cli.stdin_buf;
}

void od2_ui_cli_interact(struct od2_page *page)
{
	int c;

	/* Make stdin non-blocking. Do it right before reading data instead of
	 * only once during initialization, because commands running in the
	 * install steps may change it back to blocking. */
	fcntl(0, F_SETFL, O_NONBLOCK);

	do {
		c = getchar();
		if (c < 0)
			return;

		switch (c) {
		case '\n':
			/* Enter pressed */
			*g_cli.stdin_pos = '\0';
			stdin_line_reset();
			if (g_cli.stdin_line_cb)
				g_cli.stdin_line_cb(page, g_cli.stdin_buf);
			return;
		default:
			*g_cli.stdin_pos = c;
			g_cli.stdin_pos++;
			break;
		}

		if (stdin_line_len() >= STDIN_LINE_MAXLEN) {
			printf("\nERROR: max input len %i reached!\n",
			       STDIN_LINE_MAXLEN);
			stdin_line_reset();
			what_now_ask(page);
			return;
		}

	} while (true);
}
