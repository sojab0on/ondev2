/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once

#define UNUSED(x) ((void)x)
