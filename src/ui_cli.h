/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include "pages.h"

void od2_ui_cli_show_page(struct od2_page *page);
void od2_ui_cli_interact(struct od2_page *page);
